var path = require('path');
module.exports = {
    devtool: 'inline-source-map',
    entry: './src/basicTypes.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    }
};
//# sourceMappingURL=webpack.config.js.map