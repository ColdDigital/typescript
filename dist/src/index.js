"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = require("lodash");
function component() {
    var element = document.createElement('div');
    // Lodash, now imported by this script
    element.innerHTML = lodash_1.default.join(['Hello', 'webpack'], ' ');
    return element;
}
document.body.appendChild(component());
//# sourceMappingURL=index.js.map