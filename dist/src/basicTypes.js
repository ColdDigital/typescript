var isDone = false;
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
var color = "blue";
color = 'red';
var fullName = "Bob Bobbington";
var age = 37;
//let sentence: string = `Hello, my name is ${ fullName }.
//I'll be ${ age + 1 } years old next month.`;
var sentence = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log(sentence);
var list = [1, 2, 3];
//let list: Array<number> = [1, 2, 3];
// Declare a tuple type
var x;
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error
console.log("1");
console.log(x[0].substr(1)); // OK, word starts from 0, so h is 0, e is 1, etc. INCLUDES the letter in that
//position, so if 2 (l), it will include l, so therefore output = "llo"
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr', basically its not a string
console.log("2");
x[3] = "world"; // OK, 'string' can be assigned to 'string | number'
console.log("3");
x[4] = "Its me";
x[5] = 1000;
x[6] = 3959;
console.log("kek");
var lol = x[5].toString();
console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'
console.log(typeof lol === "string");
console.log("4");
//x[6] = true; // Error, 'boolean' isn't 'string | number'
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
// enum Color {Red = 1, Green, Blue}
// let c: Color = Color.Green;
//enum Color {Red = 1, Green = 2, Blue = 4}
//let c: Color = Color.Green;
// enum Color {Red = 1, Green, Blue}
// let colorName: string = Color[2];
//
// alert(colorName); 
//# sourceMappingURL=basicTypes.js.map