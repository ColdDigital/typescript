/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var isDone = false;
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
var color = "blue";
color = 'red';
var fullName = "Bob Bobbington";
var age = 37;
//let sentence: string = `Hello, my name is ${ fullName }.
//I'll be ${ age + 1 } years old next month.`;
var sentence = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log(sentence);
var list = [1, 2, 3];
//let list: Array<number> = [1, 2, 3];
// Declare a tuple type
var x;
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error
console.log("1");
console.log(x[0].substr(1)); // OK, word starts from 0, so h is 0, e is 1, etc. INCLUDES the letter in that
//position, so if 2 (l), it will include l, so therefore output = "llo"
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr', basically its not a string
console.log("2");
x[3] = "world"; // OK, 'string' can be assigned to 'string | number'
console.log("3");
x[4] = "Its me";
x[5] = 1000;
x[6] = 3959;
console.log("kek");
var lol = x[5].toString();
console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'
console.log(typeof lol === "string");
console.log("4");
//x[6] = true; // Error, 'boolean' isn't 'string | number'
console.log("Mac");
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
var Color1;
(function (Color1) {
    Color1[Color1["Red"] = 1] = "Red";
    Color1[Color1["Green"] = 2] = "Green";
    Color1[Color1["Blue"] = 3] = "Blue";
})(Color1 || (Color1 = {}));
var c1 = Color1.Green;
var Color2;
(function (Color2) {
    Color2[Color2["Red"] = 1] = "Red";
    Color2[Color2["Green"] = 2] = "Green";
    Color2[Color2["Blue"] = 4] = "Blue";
})(Color2 || (Color2 = {}));
var c2 = Color2.Green;
var Color3;
(function (Color3) {
    Color3[Color3["Red"] = 1] = "Red";
    Color3[Color3["Green"] = 2] = "Green";
    Color3[Color3["Blue"] = 3] = "Blue";
})(Color3 || (Color3 = {}));
var colorName = Color3[2]; //If red = 1, starts counting from 1, so array starts at 1, then green (2), then Blue
//(3)
console.log(colorName);
alert(colorName);
//Any
var notSure = 6;
notSure = "string";
notSure = false;
var notSure2 = 4;
//notSure.ifItExists();
//notSure.toFixed();
var prettySure = 4;
var list2 = [1, true, "free"];
list[1] = 100;
//void
function warnUser() {
    alert("Warning message");
}
var unusable = undefined; // can only assign null or undefined to null.
var u = undefined;
var n = null;
// : never //When type guards of variables can never be true
//Used for functions that never return or throw an exception
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
// Inferred return type is never
function fail() {
    return error("Something failed");
}
// Function returning never must have unreachable end point
function infiniteLoop() {
    while (true) {
    }
}
//Type assertions, trust me i know what im doing
var someString = "apple tree on the street";
console.log(someString);
var lengthOfSomeString = someString.length;
console.log(lengthOfSomeString);
var randomString = "pineapple pen apple pen";
console.log(randomString);
var lengthOfRandomString = randomString.length;
console.log(lengthOfRandomString);


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgMjEyZDE4ODVhMjQ1YTI3M2RjNzciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Jhc2ljVHlwZXMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhDQUE4QyxXQUFXO0FBQ3pELFlBQVksVUFBVTtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0Esb0JBQW9CO0FBQ3BCO0FBQ0EsNEJBQTRCO0FBQzVCO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsc0JBQXNCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsd0JBQXdCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsd0JBQXdCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsd0JBQXdCO0FBQ3pCLDBCQUEwQjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgMjEyZDE4ODVhMjQ1YTI3M2RjNzciLCJ2YXIgaXNEb25lID0gZmFsc2U7XG52YXIgZGVjaW1hbCA9IDY7XG52YXIgaGV4ID0gMHhmMDBkO1xudmFyIGJpbmFyeSA9IDEwO1xudmFyIG9jdGFsID0gNDg0O1xudmFyIGNvbG9yID0gXCJibHVlXCI7XG5jb2xvciA9ICdyZWQnO1xudmFyIGZ1bGxOYW1lID0gXCJCb2IgQm9iYmluZ3RvblwiO1xudmFyIGFnZSA9IDM3O1xuLy9sZXQgc2VudGVuY2U6IHN0cmluZyA9IGBIZWxsbywgbXkgbmFtZSBpcyAkeyBmdWxsTmFtZSB9LlxuLy9JJ2xsIGJlICR7IGFnZSArIDEgfSB5ZWFycyBvbGQgbmV4dCBtb250aC5gO1xudmFyIHNlbnRlbmNlID0gXCJIZWxsbywgbXkgbmFtZSBpcyBcIiArIGZ1bGxOYW1lICsgXCIuXFxuXFxuXCIgK1xuICAgIFwiSSdsbCBiZSBcIiArIChhZ2UgKyAxKSArIFwiIHllYXJzIG9sZCBuZXh0IG1vbnRoLlwiO1xuY29uc29sZS5sb2coc2VudGVuY2UpO1xudmFyIGxpc3QgPSBbMSwgMiwgM107XG4vL2xldCBsaXN0OiBBcnJheTxudW1iZXI+ID0gWzEsIDIsIDNdO1xuLy8gRGVjbGFyZSBhIHR1cGxlIHR5cGVcbnZhciB4O1xuLy8gSW5pdGlhbGl6ZSBpdFxueCA9IFtcImhlbGxvXCIsIDEwXTsgLy8gT0tcbi8vIEluaXRpYWxpemUgaXQgaW5jb3JyZWN0bHlcbi8veCA9IFsxMCwgXCJoZWxsb1wiXTsgLy8gRXJyb3JcbmNvbnNvbGUubG9nKFwiMVwiKTtcbmNvbnNvbGUubG9nKHhbMF0uc3Vic3RyKDEpKTsgLy8gT0ssIHdvcmQgc3RhcnRzIGZyb20gMCwgc28gaCBpcyAwLCBlIGlzIDEsIGV0Yy4gSU5DTFVERVMgdGhlIGxldHRlciBpbiB0aGF0XG4vL3Bvc2l0aW9uLCBzbyBpZiAyIChsKSwgaXQgd2lsbCBpbmNsdWRlIGwsIHNvIHRoZXJlZm9yZSBvdXRwdXQgPSBcImxsb1wiXG4vL2NvbnNvbGUubG9nKHhbMV0uc3Vic3RyKDEpKTsgLy8gRXJyb3IsICdudW1iZXInIGRvZXMgbm90IGhhdmUgJ3N1YnN0cicsIGJhc2ljYWxseSBpdHMgbm90IGEgc3RyaW5nXG5jb25zb2xlLmxvZyhcIjJcIik7XG54WzNdID0gXCJ3b3JsZFwiOyAvLyBPSywgJ3N0cmluZycgY2FuIGJlIGFzc2lnbmVkIHRvICdzdHJpbmcgfCBudW1iZXInXG5jb25zb2xlLmxvZyhcIjNcIik7XG54WzRdID0gXCJJdHMgbWVcIjtcbnhbNV0gPSAxMDAwO1xueFs2XSA9IDM5NTk7XG5jb25zb2xlLmxvZyhcImtla1wiKTtcbnZhciBsb2wgPSB4WzVdLnRvU3RyaW5nKCk7XG5jb25zb2xlLmxvZyh4WzVdLnRvU3RyaW5nKCkpOyAvLyBPSywgJ3N0cmluZycgYW5kICdudW1iZXInIGJvdGggaGF2ZSAndG9TdHJpbmcnXG5jb25zb2xlLmxvZyh0eXBlb2YgbG9sID09PSBcInN0cmluZ1wiKTtcbmNvbnNvbGUubG9nKFwiNFwiKTtcbi8veFs2XSA9IHRydWU7IC8vIEVycm9yLCAnYm9vbGVhbicgaXNuJ3QgJ3N0cmluZyB8IG51bWJlcidcbmNvbnNvbGUubG9nKFwiTWFjXCIpO1xudmFyIENvbG9yO1xuKGZ1bmN0aW9uIChDb2xvcikge1xuICAgIENvbG9yW0NvbG9yW1wiUmVkXCJdID0gMF0gPSBcIlJlZFwiO1xuICAgIENvbG9yW0NvbG9yW1wiR3JlZW5cIl0gPSAxXSA9IFwiR3JlZW5cIjtcbiAgICBDb2xvcltDb2xvcltcIkJsdWVcIl0gPSAyXSA9IFwiQmx1ZVwiO1xufSkoQ29sb3IgfHwgKENvbG9yID0ge30pKTtcbnZhciBjID0gQ29sb3IuR3JlZW47XG52YXIgQ29sb3IxO1xuKGZ1bmN0aW9uIChDb2xvcjEpIHtcbiAgICBDb2xvcjFbQ29sb3IxW1wiUmVkXCJdID0gMV0gPSBcIlJlZFwiO1xuICAgIENvbG9yMVtDb2xvcjFbXCJHcmVlblwiXSA9IDJdID0gXCJHcmVlblwiO1xuICAgIENvbG9yMVtDb2xvcjFbXCJCbHVlXCJdID0gM10gPSBcIkJsdWVcIjtcbn0pKENvbG9yMSB8fCAoQ29sb3IxID0ge30pKTtcbnZhciBjMSA9IENvbG9yMS5HcmVlbjtcbnZhciBDb2xvcjI7XG4oZnVuY3Rpb24gKENvbG9yMikge1xuICAgIENvbG9yMltDb2xvcjJbXCJSZWRcIl0gPSAxXSA9IFwiUmVkXCI7XG4gICAgQ29sb3IyW0NvbG9yMltcIkdyZWVuXCJdID0gMl0gPSBcIkdyZWVuXCI7XG4gICAgQ29sb3IyW0NvbG9yMltcIkJsdWVcIl0gPSA0XSA9IFwiQmx1ZVwiO1xufSkoQ29sb3IyIHx8IChDb2xvcjIgPSB7fSkpO1xudmFyIGMyID0gQ29sb3IyLkdyZWVuO1xudmFyIENvbG9yMztcbihmdW5jdGlvbiAoQ29sb3IzKSB7XG4gICAgQ29sb3IzW0NvbG9yM1tcIlJlZFwiXSA9IDFdID0gXCJSZWRcIjtcbiAgICBDb2xvcjNbQ29sb3IzW1wiR3JlZW5cIl0gPSAyXSA9IFwiR3JlZW5cIjtcbiAgICBDb2xvcjNbQ29sb3IzW1wiQmx1ZVwiXSA9IDNdID0gXCJCbHVlXCI7XG59KShDb2xvcjMgfHwgKENvbG9yMyA9IHt9KSk7XG52YXIgY29sb3JOYW1lID0gQ29sb3IzWzJdOyAvL0lmIHJlZCA9IDEsIHN0YXJ0cyBjb3VudGluZyBmcm9tIDEsIHNvIGFycmF5IHN0YXJ0cyBhdCAxLCB0aGVuIGdyZWVuICgyKSwgdGhlbiBCbHVlXG4vLygzKVxuY29uc29sZS5sb2coY29sb3JOYW1lKTtcbmFsZXJ0KGNvbG9yTmFtZSk7XG4vL0FueVxudmFyIG5vdFN1cmUgPSA2O1xubm90U3VyZSA9IFwic3RyaW5nXCI7XG5ub3RTdXJlID0gZmFsc2U7XG52YXIgbm90U3VyZTIgPSA0O1xuLy9ub3RTdXJlLmlmSXRFeGlzdHMoKTtcbi8vbm90U3VyZS50b0ZpeGVkKCk7XG52YXIgcHJldHR5U3VyZSA9IDQ7XG52YXIgbGlzdDIgPSBbMSwgdHJ1ZSwgXCJmcmVlXCJdO1xubGlzdFsxXSA9IDEwMDtcbi8vdm9pZFxuZnVuY3Rpb24gd2FyblVzZXIoKSB7XG4gICAgYWxlcnQoXCJXYXJuaW5nIG1lc3NhZ2VcIik7XG59XG52YXIgdW51c2FibGUgPSB1bmRlZmluZWQ7IC8vIGNhbiBvbmx5IGFzc2lnbiBudWxsIG9yIHVuZGVmaW5lZCB0byBudWxsLlxudmFyIHUgPSB1bmRlZmluZWQ7XG52YXIgbiA9IG51bGw7XG4vLyA6IG5ldmVyIC8vV2hlbiB0eXBlIGd1YXJkcyBvZiB2YXJpYWJsZXMgY2FuIG5ldmVyIGJlIHRydWVcbi8vVXNlZCBmb3IgZnVuY3Rpb25zIHRoYXQgbmV2ZXIgcmV0dXJuIG9yIHRocm93IGFuIGV4Y2VwdGlvblxuLy8gRnVuY3Rpb24gcmV0dXJuaW5nIG5ldmVyIG11c3QgaGF2ZSB1bnJlYWNoYWJsZSBlbmQgcG9pbnRcbmZ1bmN0aW9uIGVycm9yKG1lc3NhZ2UpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG59XG4vLyBJbmZlcnJlZCByZXR1cm4gdHlwZSBpcyBuZXZlclxuZnVuY3Rpb24gZmFpbCgpIHtcbiAgICByZXR1cm4gZXJyb3IoXCJTb21ldGhpbmcgZmFpbGVkXCIpO1xufVxuLy8gRnVuY3Rpb24gcmV0dXJuaW5nIG5ldmVyIG11c3QgaGF2ZSB1bnJlYWNoYWJsZSBlbmQgcG9pbnRcbmZ1bmN0aW9uIGluZmluaXRlTG9vcCgpIHtcbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgIH1cbn1cbi8vVHlwZSBhc3NlcnRpb25zLCB0cnVzdCBtZSBpIGtub3cgd2hhdCBpbSBkb2luZ1xudmFyIHNvbWVTdHJpbmcgPSBcImFwcGxlIHRyZWUgb24gdGhlIHN0cmVldFwiO1xuY29uc29sZS5sb2coc29tZVN0cmluZyk7XG52YXIgbGVuZ3RoT2ZTb21lU3RyaW5nID0gc29tZVN0cmluZy5sZW5ndGg7XG5jb25zb2xlLmxvZyhsZW5ndGhPZlNvbWVTdHJpbmcpO1xudmFyIHJhbmRvbVN0cmluZyA9IFwicGluZWFwcGxlIHBlbiBhcHBsZSBwZW5cIjtcbmNvbnNvbGUubG9nKHJhbmRvbVN0cmluZyk7XG52YXIgbGVuZ3RoT2ZSYW5kb21TdHJpbmcgPSByYW5kb21TdHJpbmcubGVuZ3RoO1xuY29uc29sZS5sb2cobGVuZ3RoT2ZSYW5kb21TdHJpbmcpO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9zcmMvYmFzaWNUeXBlcy5qc1xuLy8gbW9kdWxlIGlkID0gMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiXSwic291cmNlUm9vdCI6IiJ9