let isDone: boolean = false;

let decimal: number = 6;
let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744;

let color: string = "blue";
color = 'red';

let fullName: string = `Bob Bobbington`;
let age: number = 37;
//let sentence: string = `Hello, my name is ${ fullName }.

//I'll be ${ age + 1 } years old next month.`;

let sentence: string = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log(sentence);

let list: number[] = [1, 2, 3];

//let list: Array<number> = [1, 2, 3];


// Declare a tuple type
let x: [string, number];
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error

console.log("1");
console.log(x[0].substr(1)); // OK, word starts from 0, so h is 0, e is 1, etc. INCLUDES the letter in that
//position, so if 2 (l), it will include l, so therefore output = "llo"
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr', basically its not a string

console.log("2");
x[3] = "world"; // OK, 'string' can be assigned to 'string | number'
console.log("3");
x[4] = "Its me"
x[5] = 1000
x[6] = 3959
console.log("kek");
let lol = x[5].toString();
console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'
console.log(typeof lol === "string");
console.log("4");
//x[6] = true; // Error, 'boolean' isn't 'string | number'

console.log("Mac");


enum Color {Red, Green, Blue}
let c: Color = Color.Green;


enum Color1 {Red = 1, Green, Blue}
let c1: Color1 = Color1.Green;

enum Color2 {Red = 1, Green = 2, Blue = 4}
let c2: Color2 = Color2.Green;

enum Color3 {Red = 1, Green, Blue}
let colorName: string = Color3[2]; //If red = 1, starts counting from 1, so array starts at 1, then green (2), then Blue
//(3)
console.log(colorName);


alert(colorName);

//Any

let notSure: any = 6;
notSure = "string";
notSure = false;

let notSure2: any = 4;
//notSure.ifItExists();
//notSure.toFixed();

let prettySure: Object = 4;

let list2: any[] = [1, true, "free"];

list[1] = 100;

//void

function warnUser(): void {
    alert("Warning message")
}


let unusable: void = undefined; // can only assign null or undefined to void.

let u: undefined = undefined;
let n: null = null;

// : never //When type guards of variables can never be true
//Used for functions that never return or throw an exception

// Function returning never must have unreachable end point
function error (message: string): never {
    throw new Error(message);
}

// Inferred return type is never
function fail () {
    return error("Something failed");
}

 // Function returning never must have unreachable end point
function infiniteLoop(): never {
    while(true) {

    }
}

//Type assertions, trust me i know what im doing

let someString: any = "apple tree on the street";
console.log(someString);

let lengthOfSomeString: number = (someString as string).length;
console.log(lengthOfSomeString);

//I know that the input will be a string
let randomString: string = "pineapple pen apple pen";
console.log(randomString);

let lengthOfRandomString: number = randomString.length;
console.log(lengthOfRandomString);

