var isDone = false;
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
var color = "blue";
color = 'red';
var fullName = "Bob Bobbington";
var age = 37;
//let sentence: string = `Hello, my name is ${ fullName }.
//I'll be ${ age + 1 } years old next month.`;
var sentence = "Hello, my name is " + fullName + ".\n\n" +
    "I'll be " + (age + 1) + " years old next month.";
console.log(sentence);
var list = [1, 2, 3];
//let list: Array<number> = [1, 2, 3];
// Declare a tuple type
var x;
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
//x = [10, "hello"]; // Error
console.log("1");
console.log(x[0].substr(1)); // OK, word starts from 0, so h is 0, e is 1, etc. INCLUDES the letter in that
//position, so if 2 (l), it will include l, so therefore output = "llo"
//console.log(x[1].substr(1)); // Error, 'number' does not have 'substr', basically its not a string
console.log("2");
x[3] = "world"; // OK, 'string' can be assigned to 'string | number'
console.log("3");
x[4] = "Its me";
x[5] = 1000;
x[6] = 3959;
console.log("kek");
var lol = x[5].toString();
console.log(x[5].toString()); // OK, 'string' and 'number' both have 'toString'
console.log(typeof lol === "string");
console.log("4");
//x[6] = true; // Error, 'boolean' isn't 'string | number'
console.log("Mac");
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Green;
var Color1;
(function (Color1) {
    Color1[Color1["Red"] = 1] = "Red";
    Color1[Color1["Green"] = 2] = "Green";
    Color1[Color1["Blue"] = 3] = "Blue";
})(Color1 || (Color1 = {}));
var c1 = Color1.Green;
var Color2;
(function (Color2) {
    Color2[Color2["Red"] = 1] = "Red";
    Color2[Color2["Green"] = 2] = "Green";
    Color2[Color2["Blue"] = 4] = "Blue";
})(Color2 || (Color2 = {}));
var c2 = Color2.Green;
var Color3;
(function (Color3) {
    Color3[Color3["Red"] = 1] = "Red";
    Color3[Color3["Green"] = 2] = "Green";
    Color3[Color3["Blue"] = 3] = "Blue";
})(Color3 || (Color3 = {}));
var colorName = Color3[2]; //If red = 1, starts counting from 1, so array starts at 1, then green (2), then Blue
//(3)
console.log(colorName);
alert(colorName);
//Any
var notSure = 6;
notSure = "string";
notSure = false;
var notSure2 = 4;
//notSure.ifItExists();
//notSure.toFixed();
var prettySure = 4;
var list2 = [1, true, "free"];
list[1] = 100;
//void
function warnUser() {
    alert("Warning message");
}
var unusable = undefined; // can only assign null or undefined to void.
var u = undefined;
var n = null;
// : never //When type guards of variables can never be true
//Used for functions that never return or throw an exception
// Function returning never must have unreachable end point
function error(message) {
    throw new Error(message);
}
// Inferred return type is never
function fail() {
    return error("Something failed");
}
// Function returning never must have unreachable end point
function infiniteLoop() {
    while (true) {
    }
}
//Type assertions, trust me i know what im doing
var someString = "apple tree on the street";
console.log(someString);
var lengthOfSomeString = someString.length;
console.log(lengthOfSomeString);
//I know that the input will be a string
var randomString = "pineapple pen apple pen";
console.log(randomString);
var lengthOfRandomString = randomString.length;
console.log(lengthOfRandomString);
